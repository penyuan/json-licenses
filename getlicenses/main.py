#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2021 Pen-Yuan Hsing
# SPDX-License-Identifier: AGPL-3.0-or-later

# Note that the GitLab and GitHub API responses give the same fiels for 
# permissions, conditions, and limitations which are worded in the same way.

# Python Standard Library imports
import asyncio
import json
import os
import sys

# External library imports
import aiohttp

# Filename for the input JSON list of licenses not from the APIs.
# This will be merged into the final output JSON list, and is guided by the 
# choosealicense.com format: https://github.com/github/choosealicense.com
INPUT_JSON: str = "non-API_licenses.json"
# Filename for output JSON list
OUTPUT_JSON: str = "license_list.json"

# GitHub API v3 REST endpoint for licenses
GITHUB_URL: str = "https://api.github.com/licenses"
# GitHub API query success response code
GITHUB_SUCCESS_CODE: int = 200
# GitHub query headers
GITHUB_HEADERS: dict = {
    "Accept": "application/vnd.github.v3+json"
}
# GitHub query parameters
GITHUB_PARAMETERS: dict = {
#    "featured": 0, 
    "per_page": 100, 
    "page": "1"
}

# GitHub API personal access token from environment variable `GH_PAT`
GITHUB_API_TOKEN_VAR: str = "GH_PAT"
# Define an error class when this environment variable is not found
class RequiredTokenNotFound(Exception): 
    pass

# Define a custom exception for when queries fail
class APIError(Exception): 
    pass

# GitLab API v3 REST endpoint for licenses
GITLAB_URL: str = "https://gitlab.com/api/v4/templates/licenses"
# GitLab API query success response code
GITLAB_SUCCESS_CODE: int = 200
# GitLab query parameters
GITLAB_PARAMETERS: dict = {
#    "popular": 0
}

#
# Make GitHub API query for license list
#

async def main():
    # Get environment variable holding GitHub API token
    GitHub_token: str = ""
    # Check if environment variable exists and if token looks right
    try: 
        GitHub_token = os.environ[GITHUB_API_TOKEN_VAR]
        # Then check if token string
        try: 
            assert (GitHub_token.split("_")[1].isalnum() and len(GitHub_token.split("_")[1]) == 36)
        except AssertionError as incorrect_token: 
            print(f"GitHub API token string does not look right. Exiting.", file=sys.stderr)
            sys.exit(1)
        else: 
            print(f"GitHub API token looks OK.", file=sys.stderr)
    except KeyError as no_token: # KeyError is raised when `os.environ` can't find a variable
        raise RequiredTokenNotFound(f"Required environment variable {GITHUB_API_TOKEN_VAR} not found.")
    GitHub_header: dict = GITHUB_HEADERS
    GitHub_header["Authorization"]: str = f"token {GitHub_token}"
    
    # Create empty list of licenses to populate
    license_list: list = []
    # Populate list first with licenses not from the API
    with open(INPUT_JSON, "r", encoding="utf-8") as input_file: 
        license_list = json.load(input_file)

    async with aiohttp.ClientSession() as GitHub_session:
        async with GitHub_session.get(GITHUB_URL, params = GITHUB_PARAMETERS, headers = GITHUB_HEADERS) as GitHub_response:

            if GitHub_response.status == GITHUB_SUCCESS_CODE:
                pass
            else:
                raise APIError(f"Problem with GitHub query with response code {GitHub_response.status}.")

            print("Status:", GitHub_response.status)

            response_content_bytes: bytes = await GitHub_response.content.read() # https://stackoverflow.com/a/70855537/186904
            response_content: list = json.loads(response_content_bytes.decode("utf8"))

            # Get permissions details for each license
            for license in response_content: 
                async with GitHub_session.get(license["url"], params = GITHUB_PARAMETERS, headers = GITHUB_HEADERS) as license_response: 
                    if GitHub_response.status == GITHUB_SUCCESS_CODE:
                        pass
                    else:
                        raise APIError(f"Problem with GitHub query with response code {GitHub_response.status}.")
                    
                    license_response_bytes: bytes = await license_response.content.read()
                    license_content: dict = json.loads(license_response_bytes.decode("utf8"))
                    del license_content["body"]

                    license_list.append(license_content)
    
    async with aiohttp.ClientSession() as GitLab_session: 
        async with GitLab_session.get(GITLAB_URL, params = GITLAB_PARAMETERS) as GitLab_response: 

            if GitLab_response.status == GITLAB_SUCCESS_CODE:
                pass
            else:
                raise APIError(f"Problem with GitLab query with response code {GitLab_response.status}.")

            print("Status:", GitLab_response.status)

            response_content_bytes: bytes = await GitLab_response.content.read()
            response_content: list = json.loads(response_content_bytes.decode("utf8"))

            for new_license in response_content: 
                if new_license["key"] not in [license["key"] for license in license_list]: 
                    license_list.append(new_license)
    
    # Write list to JSON file
    with open(OUTPUT_JSON, "w", encoding = "utf-8") as output_file: 
        json.dump(
            license_list, 
            output_file, 
            ensure_ascii = False, 
            indent = 4, 
            sort_keys = False)
    
    pass
                    

if __name__ == "__main__": 

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())

    sys.exit(0)